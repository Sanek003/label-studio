import csv

from flask import Flask, abort, Response
from flask import request


Token = "e66627abc010d7caebffeeba7883201b83dc25bc"

app = Flask(__name__)


@app.route('/webhook', methods=['POST'])
def func():
    if request.method == "POST":
        r = request.get_json()
        try:
            annotations = len(r.get('annotation').get("result"))
        except IndexError:
            annotations = 0
        except TypeError:
            annotations = 0
        try:
            predictions = len(r.get('prediction'))
        except IndexError:
            predictions = 0
        except TypeError:
            predictions = 0
        try:
            lead_time = r.get('annotation').get('lead_time')
        except IndexError:
            lead_time = 0.0
        except TypeError:
            lead_time = 0.0
        dif = annotations - predictions
        ident = r.get("annotation").get('id')

        with open('file.csv', 'a+') as file:
            writer = csv.writer(file)
            writer.writerow(
                [annotations, predictions, lead_time, ident, dif]
            )

        return Response(status=200)
    else:
        abort(400)


if __name__ == '__main__':
    app.run()
